using Spectrum.API;
using Spectrum.API.Interfaces.Plugins;
using Spectrum.API.Interfaces.Systems;
using Spectrum.API.Game.Vehicle;
using System;
using System.Net.Sockets;
using System.Text;
using System.Linq;

namespace Rasplight_Heat_Meter
{

    public class Entry : IPlugin, IUpdatable
    {

        public string FriendlyName => "Raspi-Light Heat Meter";
        public string Author => "Earthnuker";
        public string Contact => "earthnuker@gmail.com";
        public APILevel CompatibleAPILevel => APILevel.UltraViolet;
        private TcpClient client;
        private NetworkStream stream;

        public void Initialize(IManager manager)
        {
            Console.WriteLine("Connecting...");
            client = new TcpClient("192.168.2.119", 31337);
            stream = client.GetStream();
        }

        public void Shutdown()
        {

        }

        public static string ReverseString(string s)
        {
            char[] arr = s.ToCharArray();
            Array.Reverse(arr);
            return new string(arr);
        }

        public static byte[] StringToByteArray(string hex)
        {
            return Enumerable.Range(0, hex.Length)
                             .Where(x => x % 2 == 0)
                             .Select(x => Convert.ToByte(hex.Substring(x, 2), 16))
                             .ToArray();
        }

        public void Update()
        {
            int N_Leds = 25;
            int N_Heat = (int)Math.Round(N_Leds * LocalVehicle.HeatLevel,0);
            String sb = new string('R', N_Heat).PadLeft(N_Leds,'B');
            sb = sb + ReverseString(sb);
            sb = sb.Replace("R", "FF0000").Replace("B", "000000");
            byte[] data = StringToByteArray(sb);
            stream.Write(data, 0, data.Length);
        }
    }
}